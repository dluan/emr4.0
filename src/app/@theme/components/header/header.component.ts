import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService, AnalyticsService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    throw new Error("Method not implemented.");
  }

  @Input() position = 'normal';

  user: any;

  userMenu = [{ title: 'Thông tin cá nhân' ,icon: 'ms-Icon ms-Icon--AddFriend',}, { title: 'Danh sách bệnh án'  ,icon: 'ms-Icon ms-Icon--ReadingMode',}, { title: 'Danh sách yêu cầu'  ,icon: 'ms-Icon ms-Icon--Copy',}, { title: 'Thay đổi mật khẩu'  ,icon: 'ms-Icon ms-Icon--Lock',},{ title: 'Đăng xuất' ,icon: 'ms-Icon ms-Icon--SignOut', }];
  NotifiMenu = [{  
    title1: 'Phiếu yêu cầu',title2: 'Thẩm định bệnh án',title3: 'đã dược thẩm định thành công',maphieu:'D27288F',thoigian:'10 phút'}, {  title1: 'Phiếu yêu cầu',title2: 'Thẩm định bệnh án',title3: 'đã dược thẩm định thành công',maphieu:'D27288F',thoigian:'10 phút'}, {  title1: 'Phiếu yêu cầu',title2: 'Thẩm định bệnh án',title3: 'đã dược thẩm định thành công',maphieu:'D27288F',thoigian:'10 phút'}, ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserData,
              private analyticsService: AnalyticsService,
              private router:Router) {
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe((users: any) => this.user = users.nick);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }
  async clickLogin() {
    this.router.navigateByUrl('/auth/login');
  }
  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
  toggle = true;
  status = 'Enable'; 

enableDisableRule(job) {
    this.toggle = !this.toggle;
    this.status = this.toggle ? 'Enable' : 'Disable';
}
}
