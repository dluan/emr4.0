import { Component, OnInit } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-tom-tat-benh-an',
  templateUrl: './tom-tat-benh-an.component.html',
  styleUrls: ['./tom-tat-benh-an.component.scss']
})
export class TomTatBenhAnComponent implements OnInit {
  pdfSrc: string = './assets/images/SMR04P00801_VN_BenhAnDaLieu.pdf';

  constructor(private sidebarService: NbSidebarService,private router: Router) { }
  toggleCollapse() {
    this.sidebarService.toggle(false, 'left');
  }

  ngOnInit() {
  }

  page = 1;
  zoom = 0.6;
  stickToPage = true;
  isLoaded = false;
  pdf: any;
  showAll = false;
  incrementPage(amount: number) {

    this.page += amount;
  }
  incrementZoom(amount: number) {

    this.zoom += amount;
  }
  async tomtathoso(){
    this.router.navigateByUrl('pages/tom-tat-ho-so');
  }
}
