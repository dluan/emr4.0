import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TomTatBenhAnComponent } from './tom-tat-benh-an.component';

describe('TomTatBenhAnComponent', () => {
  let component: TomTatBenhAnComponent;
  let fixture: ComponentFixture<TomTatBenhAnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TomTatBenhAnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TomTatBenhAnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
