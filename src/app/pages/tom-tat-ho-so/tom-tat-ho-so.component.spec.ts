import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TomTatHoSoComponent } from './tom-tat-ho-so.component';

describe('TomTatHoSoComponent', () => {
  let component: TomTatHoSoComponent;
  let fixture: ComponentFixture<TomTatHoSoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TomTatHoSoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TomTatHoSoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
