import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Trang chủ',
    icon: 'ms-Icon ms-Icon--Home',
    link: '/pages/trang-chu/trang-chu-quan-tri',
    home: true,
    children: [
      {
        title: 'Quản trị',
        link: '/pages/trang-chu/trang-chu-quan-tri',
      },
      {
        title: 'Người khai thác',
        link: '/pages/trang-chu/nguoi-khai-thac',
      },
    ],
  },
  {
    title: 'Yêu cầu khai thác',
    icon: 'ms-Icon ms-Icon--Boards',
    link: '/pages/tom-tat-ho-so',

    children: [
    {
      title: 'Tạo phiếu yêu cầu',
      link: '/pages/tables/tao-yeu-cau',
    },
    {
      title: 'Xem chi tiết phiếu yêu cầu',
      link: '/pages/thong-tin-phieu',
    },
    {
      title: 'Lịch sử phiếu yêu cầu',
      link: '/pages/lich-su-yeu-cau',
    },
    {
      title: 'Tóm tắt hồ sơ',
      link: '/pages/tom-tat-benh-an',
    },
    ],
  },
  {
    title: 'Hồ sơ bệnh án',
    icon: 'ms-Icon ms-Icon--ReadingMode',
    link: '/pages/hom',

    children: [
      {
        title: 'Nghiên cứu khoa học',
        link: '/pages/tables/nghien-cuu-khoa-hoc',
      },
      {
        title: 'Thẩm định bệnh án',
        link: '/pages/tables/nghien-cuu-khoa-hoc',
      },
      {
        title: 'Lập tóm tắt bệnh án',
        link: '/pages/tables/lap-tom-tat',
      },
      {
        title: 'Cập nhật thông tin bệnh án',
        link: '/pages/tables/nghien-cuu-khoa-hoc',
      },
    ],
  },
  {
    title: 'Quản lý yêu cầu',
    icon: 'ms-Icon ms-Icon--Copy',
    children: [
      {
        title: 'Quản lý yêu cầu',
        link: './quan-ly-yeu-cau',
      },
      {
        title: 'Lịch sử yêu cầu',
        link: './lich-su-yeu-cau',
      },
    ],
  },
  {
    title: 'Quản lý tài khoản',
    icon: 'ms-Icon ms-Icon--Group',
    children: [
      {
        title: 'Danh sách người dùng',
        link: './quan-ly-tai-khoan/danh-sach-nguoi-dung',
      },
      {
        title: 'Danh sách nhóm người dùng',
        link: './quan-ly-tai-khoan/danh-sach-nhom-nguoi-dung',
      },
      {
        title: 'Danh sách người dùng trong nhóm',
        link: './quan-ly-tai-khoan/danh-sach-nguoi-dung-trong-nhom',
      },
      {
        title: 'Danh sách tài khoản truy cập',
        link: './quan-ly-tai-khoan/danh-sach-tai-khoan-truy-cap',
      },
      {
        title: 'Phân quyền chức năng',
        link: './quan-ly-tai-khoan/phan-quyen-chuc-nang',
      },
      {
        title: 'Thông tin người dùng',
        link: './quan-ly-tai-khoan/thong-tin-nguoi-dung',
      },
      {
        title: 'Hồ sơ bệnh án',
        link: './quan-ly-tai-khoan/ho-so-benh-an',
      },
     
      {
        title: 'Thêm mới nhóm người dùng',
        link: './quan-ly-tai-khoan/them-moi-nhom-nguoi-dung',
      },
      {
        title: 'Thêm tài khoản',
        link: './quan-ly-tai-khoan/them-tai-khoan',
      },
     
      {
        title: 'Thiết lập chính sách mật khẩu',
        link: './quan-ly-tai-khoan/thiet-lap-chinh-sach-mat-khau',
      },
      {
        title: 'Thay đổi mật khẩu',
        link: './quan-ly-tai-khoan/thay-doi-mat-khau',
      }
    ],
  },
 
  {
    title: 'Quản lý truy quét',
    icon: 'ms-Icon ms-Icon--PageLock',
  },
  {
    title: 'Hệ thống báo cáo',
    icon: 'ms-Icon ms-Icon--Financial',
  },
  {
    title: 'Cài đặt',
    icon: 'ms-Icon ms-Icon--Settings',
    link: '/auth/login',
  },
];
