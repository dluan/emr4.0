
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LichSuYeuCauComponent } from './lich-su-yeu-cau.component';

const routes: Routes = [{
  path: '',
  component: LichSuYeuCauComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LichSuYeuCauRoutingModule { }

export const routedComponents = [
    LichSuYeuCauComponent,
];
