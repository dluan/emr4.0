import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LichSuYeuCauComponent } from './lich-su-yeu-cau.component';

describe('LichSuYeuCauComponent', () => {
  let component: LichSuYeuCauComponent;
  let fixture: ComponentFixture<LichSuYeuCauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LichSuYeuCauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LichSuYeuCauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
