import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';

@Component({
  selector: 'ngx-lich-su-yeu-cau',
  templateUrl: './lich-su-yeu-cau.component.html',
  styleUrls: ['./lich-su-yeu-cau.component.scss']
})
export class LichSuYeuCauComponent implements OnInit {

  settings = {
    actions: false,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'STT',
        type: 'number',
      },
      codeuser: {
        title: 'Mã phiếu ',
        type: 'number',
      },
      firstName: {
        title: 'Yêu cầu',
        type: 'string',
      },
      treatmenttype: {
        title: 'Thời gian tạo',
        type: 'string',
      },
      ReportType: {
        title: 'Trạng thái',
        type: 'string',
      },
      time: {
        title: 'Thời gian duyệt',
        type: 'string',
      },
      SickID: {
        title: 'Thời gian hết bệnh',
        type: 'number',
      },
      TreatmentTime: {
             title: 'Xem bệnh án',
             type: 'Number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  value = 'Tìm kiếm........';
  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      {
        field: 'stt',
        search: query
      },
      {
        field: 'ten_truy_cap',
        search: query
      },
      {
        field: 'ten_nguoi_dung',
        search: query
      },
      {
        field:'chuc_danh',
        search: query
      },
      {
        field: 'email',
        search: query
      },
      {
        field: 'trang_thai',
        search: query
      },
      {
        field: 'nhom_nguoi_dung',
        search: query
      },
    ], false); 
    }
  ngOnInit() {
  }

}
