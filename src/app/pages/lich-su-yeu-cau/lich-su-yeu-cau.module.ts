import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { LichSuYeuCauRoutingModule, routedComponents } from './lich-su-yeu-cai-routing.module';
import { NbInputModule, NbSelectModule } from '@nebular/theme';

@NgModule({
  imports: [
    ThemeModule,
    LichSuYeuCauRoutingModule,
    Ng2SmartTableModule,
    NbInputModule,
    NbSelectModule
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class LichSuYeuCauModule { }
