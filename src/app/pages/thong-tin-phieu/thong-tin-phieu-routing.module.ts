
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThongTinPhieuComponent } from './thong-tin-phieu.component';

const routes: Routes = [{
  path: '',
  component: ThongTinPhieuComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThongTinPhieuRoutingModule { }

export const routedComponents = [
    ThongTinPhieuComponent,
];
