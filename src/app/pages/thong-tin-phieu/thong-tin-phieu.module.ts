import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { NbInputModule } from '@nebular/theme';
import { ThongTinPhieuRoutingModule,routedComponents } from './thong-tin-phieu-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    ThongTinPhieuRoutingModule,
    NbInputModule,
    Ng2SmartTableModule
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class ThongTinPhieuModule { }
