import { icons } from 'eva-icons';
import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';

@Component({
  selector: 'ngx-thong-tin-phieu',
  templateUrl: './thong-tin-phieu.component.html',
  styleUrls: ['./thong-tin-phieu.component.scss']
})
export class ThongTinPhieuComponent implements OnInit {
  
  ngOnInit() {
  } 
  
  settings = { 
    actions: false,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'STT',
        type: 'number',
      },
      codeuser: {
        title: 'Mã người bệnh',
        type: 'number',
      },
      medicalcode: {
        title: 'Số bệnh án',
        type: 'number',
      },
      username: {
        title: 'Tên người bệnh ',
        type: 'string',
      },
      treatmenttype: {
        title: 'Loại điều trị',
        type: 'string',
      },
      medicaltype: {
        title: 'Loại bênh án',
        type: 'string',
      },
      department: {
        title: 'Chuyên khoa',
        type: 'string',
      },
      diseasecode: {
        title: 'Mã bệnh',
        type: 'number',
      },
      time: {
             title: 'Thời gian điều trị',
             type: 'Number',
      },
      version: {
        title: 'Phiên bản',
        type: 'string',
      },
      icons: {
        type: 'html',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  source1: LocalDataSource = new LocalDataSource();
  value = 'Tìm kiếm........';
  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(data);
    const data1 = this.service.get1Data();
    this.source1.load(data1);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

}
