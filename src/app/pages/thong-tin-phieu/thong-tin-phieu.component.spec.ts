import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongTinPhieuComponent } from './thong-tin-phieu.component';

describe('ThongTinPhieuComponent', () => {
  let component: ThongTinPhieuComponent;
  let fixture: ComponentFixture<ThongTinPhieuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongTinPhieuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongTinPhieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
