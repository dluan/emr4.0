import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyYeuCauComponent } from './quan-ly-yeu-cau.component';

describe('QuanLyYeuCauComponent', () => {
  let component: QuanLyYeuCauComponent;
  let fixture: ComponentFixture<QuanLyYeuCauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanLyYeuCauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanLyYeuCauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
