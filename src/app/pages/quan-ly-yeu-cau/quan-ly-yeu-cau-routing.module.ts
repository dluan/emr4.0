
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuanLyYeuCauComponent } from './quan-ly-yeu-cau.component';

const routes: Routes = [{
  path: '',
  component: QuanLyYeuCauComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuanLyYeuCauRoutingModule { }

export const routedComponents = [
    QuanLyYeuCauComponent,
];
