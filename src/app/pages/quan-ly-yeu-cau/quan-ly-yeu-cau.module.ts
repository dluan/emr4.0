import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { QuanLyYeuCauRoutingModule, routedComponents } from './quan-ly-yeu-cau-routing.module';
import { NbInputModule } from '@nebular/theme';

@NgModule({
  imports: [
    ThemeModule,
    QuanLyYeuCauRoutingModule,
    Ng2SmartTableModule,
    NbInputModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class QuanLyYeuCauModule { }
