import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-quan-ly-yeu-cau',
  templateUrl: './quan-ly-yeu-cau.component.html',
  styleUrls: ['./quan-ly-yeu-cau.component.scss']
})
export class QuanLyYeuCauComponent implements OnInit {
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }



  settings = {
    actions: false,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {

        width: '5%',
        title: 'STT',
        type: 'number',
      },
      IDPatient: {

        title: 'Mã phiếu ',
        type: 'number',
      },
      firstName: {

        title: 'Thời gian tạo',
        type: 'string',
      },
      treatmenttype: {

        title: 'Người yêu cầu',
        type: 'string',
      },
      ReportType: {

        title: 'Loại yêu cầu',
        type: 'string',
      },
      ChuyenKHoa: {

        title: 'Thời gian yêu cầu',
        type: 'string',
      },
      SickID: {

        title: 'Trạng Thái',
        type: 'String',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  value = 'Tìm kiếm........';
  constructor(private service: SmartTableData,private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  async click() {
    this.router.navigateByUrl('/pages/thong-tin-phieu');
  }
  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      {
        field: 'stt',
        search: query,
      },
      {
        field: 'ten_truy_cap',
        search: query,
      },
      {
        field: 'ten_nguoi_dung',
        search: query,
      },
      {
        field:'chuc_danh',
        search: query,
      },
      {
        field: 'email',
        search: query,
      },
      {
        field: 'trang_thai',
        search: query,
      },
      {
        field: 'nhom_nguoi_dung',
        search: query,
      },
    ], false);
    }

}
