import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NguoiKhaiThacComponent } from './nguoi-khai-thac.component';

describe('NguoiKhaiThacComponent', () => {
  let component: NguoiKhaiThacComponent;
  let fixture: ComponentFixture<NguoiKhaiThacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NguoiKhaiThacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NguoiKhaiThacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
