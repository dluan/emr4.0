import { Component,ChangeDetectionStrategy } from '@angular/core';
import { NbSearchService } from '@nebular/theme';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'nguoi-khai-thac',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './nguoi-khai-thac.component.html',
  styles: [`
    :host nb-tab {
      padding: 1.25rem;
    }
  `],
  styleUrls: ['./nguoi-khai-thac.component.scss'],
})
export class NguoiKhaiThacComponent {

  value = 'Tìm kiếm.....';
  constructor(private searchService: NbSearchService) { this.searchService.onSearchSubmit()
    .subscribe((data: any) => {
      this.value = data.term;
    });
   }
   phieuyeucautao: {stt: number , code: string,  request: string, time: string }[] = [
    { stt: 1, code: 'NCKH1505190001',  request: 'Lập tóm tắt bệnh án'  ,  time: '15/05/2019 14:15:52' },
    { stt: 2, code: 'NCKH1505190001',  request: 'Lập tóm tắt bệnh án'  ,  time: '15/05/2019 14:15:52' },
    { stt: 3, code: 'NCKH1505190001',  request: 'Nghiên cứu khoa học'  ,  time: '15/05/2019 14:15:52' },
    { stt: 4, code: 'NCKH1505190001',  request: 'Cập nhật nội dung'  ,  time: '15/05/2019 14:15:52' },
  ];
  phieuyeucauduyet: {stt: number , Code: string,  request: string, time: string, timerequest: string, user: string }[] = [
    { stt: 1, Code: 'NCKH1505190001',  request: 'Lập tóm tắt bệnh án'  ,  time: '15/05/2019 14:15:52' ,
     timerequest: '10 ngày', user: 'Bác sĩ CKI. Vương thế anh' },
    { stt: 2, Code: 'NCKH1505190001',  request: 'Lập tóm tắt bệnh án'  ,  time: '15/05/2019 14:15:52' ,
    timerequest: '10 ngày', user: 'Bác sĩ CKI. Vương thế anh' },
    { stt: 3, Code: 'NCKH1505190001',  request: 'Nghiên cứu khoa học'  ,  time: '15/05/2019 14:15:52' ,
    timerequest: '10 ngày', user: 'Bác sĩ CKI. Vương thế anh' },
    { stt: 4, Code: 'NCKH1505190001',  request: 'Cập nhật nội dung'  ,  time: '15/05/2019 14:15:52' ,
    timerequest: '10 ngày', user: 'Bác sĩ CKI. Vương thế anh' },
    { stt: 5, Code: 'NCKH1505190001',  request: 'Thẩm định bệnh án'  ,  time: '15/05/2019 14:15:52' ,
     timerequest: '10 ngày', user: 'Bác sĩ CKI. Vương thế anh' },
  ];
}
