import { NbCardModule } from '@nebular/theme';
import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';
import { ThemeModule } from '../../@theme/theme.module';
import { routedComponents, TrangchuRoutingModule } from './trangchu-routing.module';
import { EchartsBarComponent } from './trang-chu-quan-tri/echarts-bar.component';
import { EchartsMultipleXaxisComponent } from './trang-chu-quan-tri/echarts-multiple-xaxis.component';

const components = [
  EchartsBarComponent,
  EchartsMultipleXaxisComponent,
  
];

@NgModule({
  imports: [ThemeModule,TrangchuRoutingModule, NgxEchartsModule, NgxChartsModule, ChartModule,NbCardModule],
  declarations: [...routedComponents, ...components],
})
export class TrangChuModule {}
