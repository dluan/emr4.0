
import { NguoiKhaiThacComponent } from './nguoi-khai-thac/nguoi-khai-thac.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrangChuQuanTriComponent } from './trang-chu-quan-tri/trang-chu-quan-tri.component';
import {  TrangChuComponent } from './trangchu.component';

const routes: Routes = [{
  path: '',
  component: TrangChuComponent,
  children: [
    {
      path: '',
      component: TrangChuQuanTriComponent,
    },
    {
      path: 'nguoi-khai-thac',
      component: NguoiKhaiThacComponent,
    },
    {
    path: 'trang-chu-quan-tri',
    component: TrangChuQuanTriComponent,
  },],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrangchuRoutingModule { }

export const routedComponents = [
  TrangChuComponent,
  TrangChuQuanTriComponent,
  NguoiKhaiThacComponent
];
