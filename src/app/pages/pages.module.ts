

import { NbAuthComponent } from './../auth/components/auth.component';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NbMenuModule, NbCardModule, NbLayoutModule, NbAccordionModule, NbRadioModule, NbInputModule, NbButtonModule, NbSearchModule, NbSelectModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { TomTatHoSoComponent } from './tom-tat-ho-so/tom-tat-ho-so.component';
import { TomTatBenhAnComponent } from './tom-tat-benh-an/tom-tat-benh-an.component';
import { ThongTinPhieuComponent } from './thong-tin-phieu/thong-tin-phieu.component';
import { QuanLyYeuCauComponent } from './quan-ly-yeu-cau/quan-ly-yeu-cau.component';
import { LichSuYeuCauComponent } from './lich-su-yeu-cau/lich-su-yeu-cau.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NbDateFnsDateModule } from '@nebular/date-fns/date-fns.module';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    MiscellaneousModule,
    NbCardModule,
    FormsModule,
    NbLayoutModule,
    PdfViewerModule,
    NbAccordionModule,
    NbRadioModule,
    Ng2SmartTableModule,
    NbInputModule,
    NbButtonModule,
    NbSearchModule,
    NbSelectModule
  ],
  declarations: [
    PagesComponent,
    TomTatHoSoComponent,
    TomTatBenhAnComponent,
    ThongTinPhieuComponent,
    QuanLyYeuCauComponent,
    LichSuYeuCauComponent,
    
  ],
})
export class PagesModule {
}
