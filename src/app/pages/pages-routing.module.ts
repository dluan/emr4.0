import { QuanLyTaiKhoanModule } from './quan-ly-tai-khoan/quan-ly-tai-khoan.module';
import { NbAuthComponent } from './../auth/components/auth.component';
import { TrangChuModule } from './trang-chu/trang-chu.module';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { TrangChuComponent } from './trang-chu/trangchu.component';
import { TomTatBenhAnComponent } from './tom-tat-benh-an/tom-tat-benh-an.component';
import { ThongTinPhieuComponent } from './thong-tin-phieu/thong-tin-phieu.component';
import { QuanLyYeuCauComponent } from './quan-ly-yeu-cau/quan-ly-yeu-cau.component';
import { LichSuYeuCauComponent } from './lich-su-yeu-cau/lich-su-yeu-cau.component';
import { TomTatHoSoComponent } from './tom-tat-ho-so/tom-tat-ho-so.component';
import { QuanLyTaiKhoanComponent } from './quan-ly-tai-khoan/quan-ly-tai-khoan.component';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'trang-chu',
      loadChildren: () => import('./trang-chu/trang-chu.module')
      .then(m => m.TrangChuModule),
    },
    {
      path:'quan-ly-tai-khoan',
      loadChildren: () => import('./quan-ly-tai-khoan/quan-ly-tai-khoan.module')
      .then(m => m.QuanLyTaiKhoanModule),
    },
    {
      path: 'tables',
      loadChildren: () => import('./tables/tables.module')
      .then(m => m.TablesModule),
    },
    {
      path: 'tom-tat-benh-an',
      component: TomTatBenhAnComponent,
    },
    {
      path: 'thong-tin-phieu',
      component: ThongTinPhieuComponent,
    },
    {
      path: 'quan-ly-yeu-cau',
      component: QuanLyYeuCauComponent,
    },
    {
      path: 'lich-su-yeu-cau',
      component: LichSuYeuCauComponent,
    }, 
    {
      path: 'tom-tat-ho-so',
      component: TomTatHoSoComponent,
    },
   
    {
      path: '**',
      loadChildren: () => import('./trang-chu/trang-chu.module')
      .then(m => m.TrangChuModule),
    },
    {
      path: '',
      loadChildren: () => import('./trang-chu/trang-chu.module')
      .then(m => m.TrangChuModule),
    },
  ],
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
