import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhanQuyenChucNangComponent } from './phan-quyen-chuc-nang.component';
import { NbListModule, NbCardModule, NbSearchModule, NbAccordionModule, NbCheckboxModule } from '@nebular/theme';

@NgModule({
  imports: [
    CommonModule,
    NbListModule,
    NbCardModule,
    NbSearchModule,
    NbCheckboxModule,
    NbAccordionModule,
  ],
  declarations: [PhanQuyenChucNangComponent],
  exports: [PhanQuyenChucNangComponent],
})
export class PhanQuyenChucNangModule { }
