export const chucnang: string[] = [
  'Ban Giám Đốc',
  'Bác sĩ ngoại trú',
  'Bác sĩ nội trú',
  'Thư ký y khoa ngoại trú',
  'Điều dưỡng ngoại trú',
  'Bác sĩ cấp cứu',
  'Quản trị hệ thống',
  'Bác sĩ chuẩn đoán hình ảnh',
  'Kỹ thuật viên xét nghiệm',
];
