import { icons } from 'eva-icons';
import { Component } from '@angular/core';
import { NbSearchService } from '@nebular/theme';

interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  name: string;
}
@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-phan-quyen-chuc-nang',
  templateUrl: './phan-quyen-chuc-nang.component.html',
  styleUrls: ['./phan-quyen-chuc-nang.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class PhanQuyenChucNangComponent {
  customColumn = 'name';
  allColumns = [ this.customColumn ];

  data: TreeNode<FSEntry>[] = [
    {
      data: { name: 'Tạo yêu cầu'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Lịch sử yêu cầu'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Danh sách yêu cầu chờ duyệt'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Quản lý yêu cầu'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Danh sách tài khoản truy cập'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Danh sách người dùng'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Danh sách nhóm người dùng'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Thiết lập chính sách mật khẩu'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Danh sách chức năng'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Quản lý truy vết'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
    {
      data: { name: 'Hệ thống báo cáo'},
      children: [
        { data: { name: 'Duyệt' } },
        { data: { name: 'Từ chối'} },
      ],
    },
  ];
  value = 'Tìm kiếm.....';
  constructor(private searchService: NbSearchService) {
    this.searchService.onSearchSubmit()
// tslint:disable-next-line: no-shadowed-variable
    .subscribe((data: any) => {
      this.value = data.term;
    });
  }

  users: { name: string, title: string }[] = [
    { name: 'Ban Giám Đốc', title: 'Nurse' },
    { name: 'Bác sĩ ngoại trú', title: 'Doctor of Medicine' },
    { name: 'Bác sĩ nội trú', title: 'Janitor' },
    { name: 'Thư ký y khoa ngoại trú', title: 'Doctor of Medicine' },
    { name: 'Điều dưỡng ngoại trú', title: 'Carpenter and photographer' },
    { name: 'Bác sĩ cấp cứu', title: 'Nurse' },
    { name: 'Quản trị hệ thống', title: 'Doctor of Medicine' },
    { name: 'Bác sĩ chuẩn đoán hình ảnh', title: 'Janitor' },
    { name: 'Kỹ thuật viên xét nghiệm', title: 'Doctor of Medicine' },
    { name: 'Ban Giám Đốc', title: 'Nurse' },
    { name: 'Bác sĩ ngoại trú', title: 'Doctor of Medicine' },
    { name: 'Bác sĩ nội trú', title: 'Janitor' },
    { name: 'Thư ký y khoa ngoại trú', title: 'Doctor of Medicine' },
    { name: 'Điều dưỡng ngoại trú', title: 'Carpenter and photographer' },
    { name: 'Bác sĩ cấp cứu', title: 'Nurse' },
    { name: 'Quản trị hệ thống', title: 'Doctor of Medicine' },
    { name: 'Bác sĩ chuẩn đoán hình ảnh', title: 'Janitor' },
    { name: 'Kỹ thuật viên xét nghiệm', title: 'Doctor of Medicine' },
  ];
}

