import { DanhSachNguoiDungComponent } from './danh-sach-nguoi-dung/danh-sach-nguoi-dung.component';
import { PhanQuyenChucNangComponent } from './phan-quyen-chuc-nang/phan-quyen-chuc-nang.component';
import { QuanLyTaiKhoanComponent } from './quan-ly-tai-khoan.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThongTinNguoiDungComponent } from './thong-tin-nguoi-dung/thong-tin-nguoi-dung.component';
import { HoSoBenhAnComponent } from './ho-so-benh-an/ho-so-benh-an.component';
import { DanhSachNhomNguoiDungComponent } from './danh-sach-nhom-nguoi-dung/danh-sach-nhom-nguoi-dung.component';
import { ThemMoiNhomNguoiDungComponent } from './them-moi-nhom-nguoi-dung/them-moi-nhom-nguoi-dung.component';
import { DanhSachTaiKhoanTruyCapComponent } from './danh-sach-tai-khoan-truy-cap/danh-sach-tai-khoan-truy-cap.component';
import { ThemTaiKhoanComponent } from './them-tai-khoan/them-tai-khoan.component';
import { ThietLapChinhSachMatKhauComponent } from './thiet-lap-chinh-sach-mat-khau/thiet-lap-chinh-sach-mat-khau.component';
import { ThayDoiMatKhauComponent } from './thay-doi-mat-khau/thay-doi-mat-khau.component';
import { DanhSachNguoiDungTrongNhomComponent } from './danh-sach-nguoi-dung-trong-nhom/danh-sach-nguoi-dung-trong-nhom.component';

const routes: Routes = [{
  path: '',
  component: QuanLyTaiKhoanComponent,
  children: [{
    path: 'phan-quyen-chuc-nang',
    component: PhanQuyenChucNangComponent,
  },
  {
    path: 'danh-sach-nguoi-dung',
    component: DanhSachNguoiDungComponent,
  },
  {
    path: '',
    redirectTo: 'danh-sach-nguoi-dung',
    pathMatch: 'full',
  },
  {
    path: 'thong-tin-nguoi-dung',
    component: ThongTinNguoiDungComponent,
  },
  {
    path: 'ho-so-benh-an',
    component: HoSoBenhAnComponent,
  },
  {
    path: 'danh-sach-nhom-nguoi-dung',
    component: DanhSachNhomNguoiDungComponent,
  },
  {
    path: 'them-moi-nhom-nguoi-dung',
    component: ThemMoiNhomNguoiDungComponent,
  },
  {
    path: 'danh-sach-tai-khoan-truy-cap',
    component: DanhSachTaiKhoanTruyCapComponent,
  },
  {
    path: 'them-tai-khoan',
    component: ThemTaiKhoanComponent,
  },
  {
    path: 'thiet-lap-chinh-sach-mat-khau',
    component: ThietLapChinhSachMatKhauComponent,
  },
  {
    path: 'thay-doi-mat-khau',
    component: ThayDoiMatKhauComponent,
  },
  {
    path: 'danh-sach-nguoi-dung-trong-nhom',
    component: DanhSachNguoiDungTrongNhomComponent,
  },

],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuanLyTaiKhoanRoutingModule { }

export const routedComponents = [
  QuanLyTaiKhoanComponent,
  PhanQuyenChucNangComponent,
  DanhSachNguoiDungComponent,
  ThongTinNguoiDungComponent,
  HoSoBenhAnComponent,
  DanhSachNhomNguoiDungComponent,
  ThemMoiNhomNguoiDungComponent,
  DanhSachTaiKhoanTruyCapComponent,
  ThemTaiKhoanComponent,
  ThietLapChinhSachMatKhauComponent,
  ThayDoiMatKhauComponent,
  DanhSachNguoiDungTrongNhomComponent,
];
