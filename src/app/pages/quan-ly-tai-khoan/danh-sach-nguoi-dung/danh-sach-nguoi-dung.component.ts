import { Component, Input} from '@angular/core';

import {Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table'; 
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-danh-sach-nguoi-dung',
  styleUrls: ['./danh-sach-nguoi-dung.component.scss'],
  templateUrl: './danh-sach-nguoi-dung.component.html',
  providers: [],
})
export class DanhSachNguoiDungComponent {

  settings = {
    mode: 'external',
    actions: {

      add: false,
      columnTitle: '',
      position: 'right',
    },
    columnTitle: '',
    position: 'right',


    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      stt: {
        filter: false,
        width: '5%',
        title: 'STT',
        type: 'number',
      },
      ten_nguoi_dung: {
        filter: false,
        title: 'Tên người dùng',
        type: 'string',
      },
      chuc_danh: {
        filter: false,
        title: 'Chức danh',
        type: 'string',
      },
      gioi_tinh: {
        filter: false,
        title: 'Giới tính',
        type: 'string',
      },
      email: {
        filter: false,
        title: 'E-mail',
        type: 'string',
      },
      Sodt: {
        filter: false,
        title: 'Số điện thoại',
        type: 'number',
      },
      nhom_nguoi_dung: {
        filter: false,
        title: 'Nhóm người dùng',
        type: 'string',
      },
      noi_lam_viec: {

        filter: false,
        title: 'Nơi công tác',
        type: 'string',
      },
      action: {
        filter: false,
          width: '5%',
      },
    },
  };


  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData, private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
}
onDeleteConfirm(event): void {
  if (window.confirm('Bạn có chắc muốn xóa?')) {
    event.confirm.resolve();
  } else {
    event.confirm.reject();
  }
}
onSearch(query: string = '') {
  this.source.setFilter([
    // fields we want to include in the search
    {
      field: 'stt',
      search: query,
    },
    {
      field: 'ten_nguoi_dung',
      search: query,
    },
    {
      field: 'chuc_danh',
      search: query,
    },
    {
      field: 'gioi_tinh',
      search: query,
    },
    {
      field: 'email',
      search: query,
    },
    {
      field: 'Sodt',
      search: query,
    },
    {
      field: 'nhom_nguoi_dung',
      search: query,
    },
    {
      field: 'noi_lam_viec',
      search: query,
    },
  ], false);
  }
  async themnguoidung() {
    this.router.navigateByUrl('pages/quan-ly-tai-khoan/thong-tin-nguoi-dung');
  }
  async chinhsua() {
    this.router.navigateByUrl('pages/quan-ly-tai-khoan/thong-tin-nguoi-dung');
  }
}

