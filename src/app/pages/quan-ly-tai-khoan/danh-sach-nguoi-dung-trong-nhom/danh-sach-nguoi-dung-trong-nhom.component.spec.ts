/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DanhSachNguoiDungTrongNhomComponent } from './danh-sach-nguoi-dung-trong-nhom.component';

describe('DanhSachNguoiDungTrongNhomComponent', () => {
  let component: DanhSachNguoiDungTrongNhomComponent;
  let fixture: ComponentFixture<DanhSachNguoiDungTrongNhomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanhSachNguoiDungTrongNhomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhSachNguoiDungTrongNhomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
