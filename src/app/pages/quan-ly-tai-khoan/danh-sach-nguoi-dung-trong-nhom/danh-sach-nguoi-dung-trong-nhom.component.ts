import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table'; 
@Component({
  selector: 'app-danh-sach-nguoi-dung-trong-nhom',
  templateUrl: './danh-sach-nguoi-dung-trong-nhom.component.html',
  styleUrls: ['./danh-sach-nguoi-dung-trong-nhom.component.scss']
})
export class DanhSachNguoiDungTrongNhomComponent {

  formControl = new FormControl(new Date());
  ngModelDate = new Date();
  settings = {
    mode: 'external',
    actions: false,
    selectMode: 'multi',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      stt: {
        filter: false,
        width: '15%',
        title: 'STT',
        type: 'number',
      },
      ten_truy_cap: {
        filter: false,
        width: '20%',
        title: 'Tên truy cập',
        type: 'string',
      },
      ten_nguoi_dung: {
        filter: false,
        width: '20%',
        title: 'Tên người dùng',
        type: 'string',
      },
      chuc_danh: {
        filter: false,
        width: '20%',
        title: 'Chức danh',
        type: 'string',
      },
      gioi_tinh: {
        filter: false,
        width: '20%',
        title: 'Giới tính',
        type: 'string',
      },

    },
  };
  source: LocalDataSource = new LocalDataSource();
  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(data);
}
onDeleteConfirm(event): void {
  if (window.confirm('Bạn có chắc muốn xóa?')) {
    event.confirm.resolve();
  } else {
    event.confirm.reject();
  }
}
onSearch(query: string = '') {
  this.source.setFilter([
    // fields we want to include in the search
    {
      field: 'stt',
      search: query,
    },
    {
      field: 'ten_truy_cap',
      search: query,
    },
    {
      field: 'ten_nguoi_dung',
      search: query,
    },
    {
      field:'chuc_danh',
      search: query,
    },
    {
      field: 'gioi_tinh',
      search: query,
    },

  ], false);
  }
  public onUserRowSelect(event) {
    var selectedRows = event.selected;
    console.log(selectedRows);
  }
}
