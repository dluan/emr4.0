import { Component, ViewChild, TemplateRef } from '@angular/core';
import { NbSearchService } from '@nebular/theme';
import { NbWindowService } from '@nebular/theme';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-phan-nhom-nguoi-dung',
  templateUrl: './phan-nhom-nguoi-dung.component.html',
  styleUrls: ['./phan-nhom-nguoi-dung.component.scss'],
})
export class PhanNhomNguoiDungComponent {
  @ViewChild('contentTemplate',{static:true}) contentTemplate: TemplateRef<any>;
  value = 'Tìm kiếm.....';
  constructor(private searchService: NbSearchService, private windowService: NbWindowService) {
    this.searchService.onSearchSubmit()
// tslint:disable-next-line: no-shadowed-variable
    .subscribe((data: any) => {
      this.value = data.term;
    });
  }
  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Danh sách nhóm người dùng',
      },
    );
  }
 functions: { name: string, note: string }[] = [
    { name: 'Điều dưỡng nội trú', note: 'Nguyễn An' },
    { name: 'Thu ngân ngoại trú', note: 'Nguyễn Đình Chiện' },
    { name: 'Thu ngận nội trú', note: 'Nguyễn Đức Tú' },
    { name: 'Kế toán trưởng', note: 'Trương Đình Luân' },
    { name: 'Trưởng khoa dược', note: 'admin' },
    { name: 'Hành chính', note: 'Võ Minh Duy' },
    { name: 'Bác sĩ lâm sàng', note: 'Võ Trí Dũng' },
    { name: 'Kế toán mua hàng', note: 'Nguyễn Đức Tú' },
    { name: 'Dược sĩ ngoại trú', note: 'Bùi Thị Yến' },
    { name: 'Dược sĩ nội trú', note: 'Nguyễn An' },
    { name: 'Kĩ thuật viên chuẩn đoán hình ảnh', note: 'Nguyễn Đình Chiện' },
    { name: 'Lãnh đạo', note: 'Nguyễn Đức Tú' },
  ];
  users: {stt: number , username: string, name: string }[] = [
  { stt: 1, username: 'nguyenan', name: 'Nguyễn An' },
  { stt: 2, username: 'chiennd8', name: 'Nguyễn Đình Chiện' },
  { stt: 3, username: 'tund9', name: 'Nguyễn Đức Tú' },
  { stt: 4, username: 'luantd4', name: 'Trương Đình Luân' },
  { stt: 5, username: 'admin', name: 'admin' },
  { stt: 6, username: 'duyvm04', name: 'Võ Minh Duy' },
  { stt: 7, username: 'dungvt', name: 'Võ Trí Dũng' },
  { stt: 8, username: 'abcss1', name: 'Nguyễn Đức Tú' },
  { stt: 9, username: 'yenbt', name: 'Bùi Thị Yến' },
  { stt: 10, username: 'nguyenan01', name: 'Nguyễn An' },
  { stt: 11, username: 'abc09', name: 'Nguyễn Đình Chiện' },
  { stt: 12, username: 'acc01', name: 'Nguyễn Đức Tú' },
  { stt: 13, username: 'acc05', name: 'Nguyễn Đình Chiện' },
  { stt: 14, username: 'dungvt30', name: 'Võ Trí Dũng' },
  { stt: 15, username: 'annd3', name: 'Nguyễn An' },
  { stt: 16, username: 'chiennd1', name: 'Nguyễn Đình Chiện' },
  { stt: 17, username: 'adtund9', name: 'Nguyễn Đức Tú' },
  { stt: 18, username: 'trungtnk', name: 'Nguyễn Đình Chiện' },
];
}
