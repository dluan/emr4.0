import { Component, OnInit } from '@angular/core';
import {Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table'; 
import { Router } from '@angular/router';
@Component({
  selector: 'app-danh-sach-tai-khoan-truy-cap',
  templateUrl: './danh-sach-tai-khoan-truy-cap.component.html',
  styleUrls: ['./danh-sach-tai-khoan-truy-cap.component.scss']
})
export class DanhSachTaiKhoanTruyCapComponent  {
  settings = {
    mode: 'external',
    actions:{
      
      add: false,
      columnTitle: '',
      position: 'right',
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      stt: {
        filter: false,
        width: '5%',
        title: 'STT',
        type: 'number',
      },
      ten_truy_cap: {
        filter: false,
        title: 'Tên truy cập',
        type: 'string',
      },
      ten_nguoi_dung: {
        filter: false,
        title: 'Tên người dùng',
        type: 'string',
      },
      chuc_danh: {
        filter: false,
        title: 'Chức danh',
        type: 'string',
      },
      email: {
        filter: false,
        title: 'E-mail',
        type: 'string',
      },
      trang_thai: {
        filter: false,
        title: 'Trạng thái',
        type: 'string',
      },
      nhom_nguoi_dung: {
        filter: false,
        title: 'Nhóm người dùng',
        type: 'string',
      },
    },
  };
  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData, private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
}
onDeleteConfirm(event): void {
  if (window.confirm('Are you sure you want to delete?')) {
    event.confirm.resolve();
  } else {
    event.confirm.reject();
  }
}
onSearch(query: string = '') {
  this.source.setFilter([
    // fields we want to include in the search
    {
      field: 'stt',
      search: query
    },
    {
      field: 'ten_truy_cap',
      search: query
    },
    {
      field: 'ten_nguoi_dung',
      search: query
    },
    {
      field:'chuc_danh',
      search: query
    },
    {
      field: 'email',
      search: query
    },
    {
      field: 'trang_thai',
      search: query
    },
    {
      field: 'nhom_nguoi_dung',
      search: query
    },
  ], false); 
  }
  async themtaikhoan(){
    this.router.navigateByUrl('pages/quan-ly-tai-khoan/them-tai-khoan');
  }
  async chinhsua(){
    this.router.navigateByUrl('pages/quan-ly-tai-khoan/them-tai-khoan');
  }
}
