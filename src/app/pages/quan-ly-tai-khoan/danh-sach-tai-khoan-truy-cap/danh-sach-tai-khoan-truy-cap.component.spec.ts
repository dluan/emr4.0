/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DanhSachTaiKhoanTruyCapComponent } from './danh-sach-tai-khoan-truy-cap.component';

describe('DanhSachTaiKhoanTruyCapComponent', () => {
  let component: DanhSachTaiKhoanTruyCapComponent;
  let fixture: ComponentFixture<DanhSachTaiKhoanTruyCapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanhSachTaiKhoanTruyCapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhSachTaiKhoanTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
