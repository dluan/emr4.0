import { Component, TemplateRef,ViewChild,Input  } from '@angular/core';
import { NbWindowModule,NbWindowService,   } from '@nebular/theme';
@Component({
  selector: 'app-thong-tin-nguoi-dung',
  templateUrl: './thong-tin-nguoi-dung.component.html',
  styleUrls: ['./thong-tin-nguoi-dung.component.scss']
})
export class ThongTinNguoiDungComponent {
  @ViewChild('contentTemplate',{static:true} ) contentTemplate: TemplateRef<any>;

  constructor(private windowService: NbWindowService, ) { }

  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      { title: 'CẤP LẠI MẬT KHẨU'},
    );
  }
  show: boolean = false;
  onchange(value) {
    if (value === 1 ) {
      this.show = false;

    }else
      {
       this.show = true;

    }
    return this.show;
    }
  }


