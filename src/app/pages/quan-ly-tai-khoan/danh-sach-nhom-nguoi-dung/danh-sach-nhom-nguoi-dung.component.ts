import { Component, OnInit } from '@angular/core';
import {Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table'; 
import { Router } from '@angular/router';
@Component({
  selector: 'app-danh-sach-nhom-nguoi-dung',
  templateUrl: './danh-sach-nhom-nguoi-dung.component.html',
  styleUrls: ['./danh-sach-nhom-nguoi-dung.component.scss']
})
export class DanhSachNhomNguoiDungComponent {

  settings = {
    mode: 'external',
    actions:{
      
      add: false,
      columnTitle: '',
      position: 'right',
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      stt: {
        filter: false,
        width: '5%',
        title: 'STT',
        type: 'number',
      },
      ten_nhom: {
        filter: false,
        title: 'Tên nhóm',
        type: 'string',
      },
      trang_thai: {
        filter: false,
        title: 'Trạng thái',
        type: 'string',
      },
      thoi_gian_hieu_luc: {
        filter: false,
        title: 'thời gian hiệu lực',
        type: 'string',
      },
      thoi_gian_het_hieu_luc: {
        filter: false,
        title: 'thời gian hết hiệu lực',
        type: 'string',
      },
     
    },
  };
  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData, private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
}
onDeleteConfirm(event): void {
  if (window.confirm('Are you sure you want to delete?')) {
    event.confirm.resolve();
  } else {
    event.confirm.reject();
  }
}
onSearch(query: string = '') {
  this.source.setFilter([
    // fields we want to include in the search
    {
      field: 'stt',
      search: query
    },
    {
      field: 'ten_nhom',
      search: query
    },
    {
      field: 'trang_thai',
      search: query
    },
    {
      field:'thoi_gian_hieu_luc',
      search: query
    },
    {
      field: 'thoi_gian_het_hieu_luc',
      search: query
    },
    
  ], false); 
  }
  async themnhomnguoidung(){
    this.router.navigateByUrl('pages/quan-ly-tai-khoan/them-moi-nhom-nguoi-dung');
  }
  async chinhsua(){
    this.router.navigateByUrl('pages/quan-ly-tai-khoan/them-moi-nhom-nguoi-dung');
  }
}


