/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ThietLapChinhSachMatKhauComponent } from './thiet-lap-chinh-sach-mat-khau.component';

describe('ThietLapChinhSachMatKhauComponent', () => {
  let component: ThietLapChinhSachMatKhauComponent;
  let fixture: ComponentFixture<ThietLapChinhSachMatKhauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThietLapChinhSachMatKhauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThietLapChinhSachMatKhauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
