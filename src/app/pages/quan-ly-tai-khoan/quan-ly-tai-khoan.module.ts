import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuanLyTaiKhoanComponent } from './quan-ly-tai-khoan.component';
import { QuanLyTaiKhoanRoutingModule, routedComponents } from './quan-ly-tai-khoan-routing.module';
import { ThemeModule } from '../../@theme/theme.module';
import { NbTreeGridModule, NbDatepickerModule, NbCheckboxModule, NbCardModule, NbSelectModule, NbSearchModule, NbAccordionModule, NbListModule, NbRadioModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbButtonModule, NbInputModule } from '@nebular/theme';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {NbMomentDateModule} from '@nebular/moment';
import {NbDateFnsDateModule} from '@nebular/date-fns';
import { SmartTableComponent } from '../tables/smart-table/smart-table.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PhanQuyenChucNangComponent } from './phan-quyen-chuc-nang/phan-quyen-chuc-nang.component';
import { DanhSachNguoiDungComponent } from './danh-sach-nguoi-dung/danh-sach-nguoi-dung.component';
import { DanhSachNguoiDungTrongNhomComponent } from './danh-sach-nguoi-dung-trong-nhom/danh-sach-nguoi-dung-trong-nhom.component';
import { ThayDoiMatKhauComponent } from './thay-doi-mat-khau/thay-doi-mat-khau.component';
import { ThietLapChinhSachMatKhauComponent } from './thiet-lap-chinh-sach-mat-khau/thiet-lap-chinh-sach-mat-khau.component';
import { ThemTaiKhoanComponent } from './them-tai-khoan/them-tai-khoan.component';
import { DanhSachTaiKhoanTruyCapComponent } from './danh-sach-tai-khoan-truy-cap/danh-sach-tai-khoan-truy-cap.component';
import { ThemMoiNhomNguoiDungComponent } from './them-moi-nhom-nguoi-dung/them-moi-nhom-nguoi-dung.component';
import { DanhSachNhomNguoiDungComponent } from './danh-sach-nhom-nguoi-dung/danh-sach-nhom-nguoi-dung.component';
import { HoSoBenhAnComponent } from './ho-so-benh-an/ho-so-benh-an.component';
import { ThongTinNguoiDungComponent } from './thong-tin-nguoi-dung/thong-tin-nguoi-dung.component';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports: [

    ReactiveFormsModule,
    ThemeModule,
    CommonModule,
    NbTreeGridModule,
    QuanLyTaiKhoanRoutingModule,
    NbDatepickerModule,
    NbCheckboxModule,
    PdfViewerModule,
    NbMomentDateModule,
    NbDateFnsDateModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbSelectModule,
    NbSearchModule,
    NbAccordionModule,
    NbListModule,
    NbRadioModule,
    NbLayoutModule,
    NbSidebarModule,
    NbMenuModule,
    NbButtonModule,
    FormsModule,
    NbDatepickerModule,
    NbInputModule,
  ],
  declarations: [QuanLyTaiKhoanComponent,
    ...routedComponents,
  ],
})
export class QuanLyTaiKhoanModule { }
