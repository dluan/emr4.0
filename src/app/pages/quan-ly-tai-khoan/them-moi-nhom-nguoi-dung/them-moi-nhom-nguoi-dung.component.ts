import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table'; 
import { Router } from '@angular/router';
@Component({
  selector: 'app-them-moi-nhom-nguoi-dung',
  templateUrl: './them-moi-nhom-nguoi-dung.component.html',
  styleUrls: ['./them-moi-nhom-nguoi-dung.component.scss']
})
export class ThemMoiNhomNguoiDungComponent {
  formControl = new FormControl(new Date());
  ngModelDate = new Date();
  settings = {
    mode: 'external',
    actions:{
      
      add: false,
      columnTitle: '',
      position: 'right',
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      stt: {
        filter: false,
        width: '5%',
        title: 'STT',
        type: 'number',
      },
      ten_truy_cap: {
        filter: false,
        title: 'Tên truy cập',
        type: 'string',
      },
      ten_nguoi_dung: {
        filter: false,
        title: 'Tên người dùng',
        type: 'string',
      },
      chuc_danh: {
        filter: false,
        title: 'Chức danh',
        type: 'string',
      },
      gioi_tinh: {
        filter: false,
        title: 'Giới tính',
        type: 'string',
      },
     
    },
  };
  source: LocalDataSource = new LocalDataSource();
  constructor(private service: SmartTableData, private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
}
onDeleteConfirm(event): void {
  if (window.confirm('Bạn có chắc muốn xóa?')) {
    event.confirm.resolve();
  } else {
    event.confirm.reject();
  }
}
onSearch(query: string = '') {
  this.source.setFilter([
    // fields we want to include in the search
    {
      field: 'stt',
      search: query
    },
    {
      field: 'ten_truy_cap',
      search: query
    },
    {
      field: 'ten_nguoi_dung',
      search: query
    },
    {
      field:'chuc_danh',
      search: query
    },
    {
      field: 'gioi_tinh',
      search: query
    },
    
  ], false); 
  }
  async chinhsuanguoidung(){
    this.router.navigateByUrl('pages/quan-ly-tai-khoan/danh-sach-nguoi-dung-trong-nhom');
  }
}
