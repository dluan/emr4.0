import { Component, OnInit } from '@angular/core';
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'app-ho-so-benh-an',
  templateUrl: './ho-so-benh-an.component.html',
  styleUrls: ['./ho-so-benh-an.component.scss']
})
export class HoSoBenhAnComponent {
  pdfSrc: string = './assets/images/SMR04P00801_VN_BenhAnDaLieu.pdf';
  constructor(private sidebarService: NbSidebarService) { }
  toggleCollapse() {
    this.sidebarService.toggle(false, 'left');
  }
  ngOnInit() {
  }
  items = [
    {
      title: 'Bệnh án',
      link: [],
    },
    {
      title: 'Y lệnh thuốc',
      link: [],
    },
    {
      title: 'Y lệnh VTYT',
      link: [],
    },
    {
      title: 'Phiếu chăm sóc',
      link: [],
    },
    {
      title: 'Xét nghiệm',
      link: [],
    },
    {
      title: 'Chuẩn đoán hình ảnh',
      
      link: [],
    },
    {
      title: 'Phẫu Thuật - Thủ thuật',
      
      link: [],
    },
    
  ];
  page = 1;
  zoom = 1;
  stickToPage = true;
  isLoaded = false;
  pdf: any;
  showAll = false;
  incrementPage(amount: number) {

    this.page += amount;
  }
  incrementZoom(amount: number) {

    this.zoom += amount;
  }
}
