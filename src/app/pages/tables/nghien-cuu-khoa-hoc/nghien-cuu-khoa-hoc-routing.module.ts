import { NghienCuuKhoaHocMainModule } from './nghien-cuu-khoa-hoc-main/nghien-cuu-khoa-hoc-main.module';
import { NghienCuuKhoaHocListModule } from './nghien-cuu-khoa-hoc-list/nghien-cuu-khoa-hoc-list.module';
import { NghienCuuKhoaHocMainComponent } from './nghien-cuu-khoa-hoc-main/nghien-cuu-khoa-hoc-main.component';
import { NghienCuuKhoaHocListComponent } from './nghien-cuu-khoa-hoc-list/nghien-cuu-khoa-hoc-list.component';
import { NghienCuuKhoaHocComponent } from './nghien-cuu-khoa-hoc.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [{
  path: '',
  component: NghienCuuKhoaHocComponent,
  children: [
    {
      path: 'nghien-cuu-khoa-hoc-list',
      component: NghienCuuKhoaHocListComponent,
    },
    {
    path: 'nghien-cuu-khoa-hoc-main',
    component: NghienCuuKhoaHocComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes), NghienCuuKhoaHocListModule, NghienCuuKhoaHocMainModule],
  exports: [RouterModule],
})
export class LapTomTatRoutingModule { }

export const routedComponents = [
  NghienCuuKhoaHocComponent,
  NghienCuuKhoaHocListComponent,
  NghienCuuKhoaHocMainComponent,
  NghienCuuKhoaHocComponent,
];
