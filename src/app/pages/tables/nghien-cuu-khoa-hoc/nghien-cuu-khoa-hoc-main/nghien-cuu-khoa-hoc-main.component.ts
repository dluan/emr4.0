import { NbSearchService } from '@nebular/theme';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-nghien-cuu-khoa-hoc-main',
  templateUrl: './nghien-cuu-khoa-hoc-main.component.html',
  styleUrls: ['./nghien-cuu-khoa-hoc-main.component.scss']
})
export class NghienCuuKhoaHocMainComponent implements OnInit {
  value = 'Tìm kiếm.....';
  constructor(private searchService: NbSearchService , private router: Router) { this.searchService.onSearchSubmit()
    .subscribe((data: any) => {
      this.value = data.term;
    });
   }
  ngOnInit() {
  }
  async click(){
    this.router.navigateByUrl('/pages/quan-ly-tai-khoan/ho-so-benh-an');
  }
}
