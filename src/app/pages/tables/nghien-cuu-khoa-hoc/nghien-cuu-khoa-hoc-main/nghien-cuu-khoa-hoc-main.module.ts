import { NbCheckboxModule } from '@nebular/theme';
import { NghienCuuKhoaHocMainComponent } from './nghien-cuu-khoa-hoc-main.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    NbCheckboxModule,
  ],
  declarations: [NghienCuuKhoaHocMainComponent],
  exports: [NghienCuuKhoaHocMainComponent],
})
export class NghienCuuKhoaHocMainModule { }
