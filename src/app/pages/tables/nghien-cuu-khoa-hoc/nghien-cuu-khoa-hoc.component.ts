import { Component, OnInit} from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { NbSearchService } from '@nebular/theme';

@Component({
  selector: 'ngx-nghien-cuu-khoa-hoc',
  templateUrl: './nghien-cuu-khoa-hoc.html',
  styleUrls: ['./nghien-cuu-khoa-hoc.scss'],
})
// tslint:disable-next-line: one-line
export class NghienCuuKhoaHocComponent implements OnInit{
  public isCard: boolean;
  public isList: boolean;
  ngOnInit() { this.isCard = true; this.isList = false; }
  public Card(): void { this.isCard = true; this.isList = false; }
  public List(): void { this.isList = true; this.isCard = false;  }
}
