import { ThemeModule } from './../../../@theme/theme.module';
import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { LapTomTatRoutingModule, routedComponents } from './nghien-cuu-khoa-hoc-routing.module';
import { NbCardModule, NbCheckboxModule } from '@nebular/theme';

@NgModule({
  imports: [
    ThemeModule,
    LapTomTatRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbCheckboxModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class NghienCuuKhoaHocModule { }
