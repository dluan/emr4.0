import { SmartTableData } from './../../../../@core/data/smart-table';
import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NbSearchService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-nghien-cuu-khoa-hoc-list',
  templateUrl: './nghien-cuu-khoa-hoc-list.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
  styleUrls: ['./nghien-cuu-khoa-hoc-list.component.scss'],
})

export class NghienCuuKhoaHocListComponent {

  settings = {
    actions: false,
    columns: {
      id: {
        title: 'STT',
        type: 'number',
      },
      codeuser: {
        title: 'Mã người bệnh',
        type: 'number',
      },
      medicalcode: {
        title: 'Số bệnh án',
        type: 'number',
      },
      username: {
        title: 'Tên người bệnh',
        type: 'string',
      },
      treatmenttype: {
        title: 'Loại điều trị',
        type: 'string',
      },
      medicaltype: {
        title: 'Loại bệnh án',
        type: 'string',
      },
      department: {
        title: 'Chuyên khoa',
        type: 'string',
      },
      diseasecode: {
        title: 'Mã bệnh',
        type: 'number',
      },
      time: {
        title: 'Thời gian điều trị',
        type: 'string',
      },
      version:{
        title: 'Phiên bản',
        type: 'number',
      },
      status: {
        title: 'Trạng thái',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  value = 'Tìm kiếm.....';
  constructor(private service: SmartTableData, private searchService: NbSearchService, private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
    this.searchService.onSearchSubmit()
// tslint:disable-next-line: no-shadowed-variable
    .subscribe((data: any) => {
      this.value = data.term;
    });
  }
  async click(){
    this.router.navigateByUrl('/pages/quan-ly-tai-khoan/ho-so-benh-an');
  }

  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      {
        field: 'id',
        search: query,
      },
      {
        field: 'codeuser',
        search: query,
      },
      {
        field: 'medicalcode',
        search: query,
      },
      {
        field:'username',
        search: query,
      },
      {
        field: 'treatmenttype',
        search: query,
      },
      {
        field: 'medicaltype',
        search: query,
      },
      {
        field: 'department',
        search: query,
      },
      {
        field: 'diseasecode',
        search: query,
      },
      {
        field: 'time',
        search: query,
      },
      {
        field: 'version',
        search: query,
      },
      {
        field: 'status',
        search: query,
      },
    ], false);
    }
}
