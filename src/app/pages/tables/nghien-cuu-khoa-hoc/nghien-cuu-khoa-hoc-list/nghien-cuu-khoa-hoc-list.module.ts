import { NghienCuuKhoaHocListComponent } from './nghien-cuu-khoa-hoc-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [NghienCuuKhoaHocListComponent],
  exports: [NghienCuuKhoaHocListComponent],
})
export class NghienCuuKhoaHocListModule { }
