import { NbCardModule, NbCheckboxModule, NbSelectModule, NbSearchModule } from '@nebular/theme';

import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './tables-routing.module';
import { TaoPhieuYeuCauComponent } from './tao-phieu-yeu-cau/tao-phieu-yeu-cau.component';

@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbCheckboxModule,
    NbSelectModule,
    NbSearchModule,
  ],
  declarations: [
    ...routedComponents,
    TaoPhieuYeuCauComponent,
  ],
})
export class TablesModule { }
