import { TaoPhieuYeuCauComponent } from './tao-phieu-yeu-cau/tao-phieu-yeu-cau.component';
import { NghienCuuKhoaHocMainComponent } from './nghien-cuu-khoa-hoc/nghien-cuu-khoa-hoc-main/nghien-cuu-khoa-hoc-main.component';
import { NghienCuuKhoaHocListComponent } from './nghien-cuu-khoa-hoc/nghien-cuu-khoa-hoc-list/nghien-cuu-khoa-hoc-list.component';
import { NghienCuuKhoaHocComponent } from './nghien-cuu-khoa-hoc/nghien-cuu-khoa-hoc.component';
import { LapTomTatMainComponent } from './lap-tom-tat/lap-tom-tat-main/lap-tom-tat-main.component';
import { LapTomTatListComponent } from './lap-tom-tat/lap-tom-tat-list/lap-tom-tat-list.component';
import { LapTomTatComponent } from './lap-tom-tat/lap-tom-tat.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TablesComponent } from './tables.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { SmartTableHomeComponent } from './smart-table-home/smart-table-home.component';


const routes: Routes = [{
  path: '',
  component: TablesComponent,
  children: [{
    path: 'smart-table',
    component: SmartTableComponent,
  },
  {
    path: 'smart-table-home',
    component: SmartTableHomeComponent,
  },
  {
    path: 'tao-yeu-cau',
    component:TaoPhieuYeuCauComponent,
  },
  {
    path: 'lap-tom-tat',
    component: LapTomTatComponent,
    children: [
      {
        path: 'lap-tom-tat-list',
        component: LapTomTatListComponent,
      },
      {
        path: 'lap-tom-tat-main',
        component: LapTomTatMainComponent,
      },
    ],
  },
  {
    path: 'nghien-cuu-khoa-hoc',
    component: NghienCuuKhoaHocComponent,
    children: [
      {
        path: 'nghien-cuu-khoa-hoc-list',
        component: NghienCuuKhoaHocListComponent,
      },
      {
        path: 'nghien-cuu-khoa-hoc-main',
        component: NghienCuuKhoaHocMainComponent,
      },
    ],
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesRoutingModule { }

export const routedComponents = [
  TablesComponent,
  LapTomTatComponent,
  LapTomTatListComponent,
  LapTomTatMainComponent,
  NghienCuuKhoaHocComponent,
  NghienCuuKhoaHocListComponent,
  NghienCuuKhoaHocMainComponent,
  SmartTableComponent,
  SmartTableHomeComponent,
];
