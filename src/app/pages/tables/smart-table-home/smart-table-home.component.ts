import { Component } from '@angular/core';
import { NbSearchService } from '@nebular/theme';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-smart-table-home',
  templateUrl: './smart-table-home.component.html',
  styleUrls: ['./smart-table-home.component.scss'],
})
// tslint:disable-next-line: one-line
export class SmartTableHomeComponent{
  value = 'Tìm kiếm.....';
  constructor(private searchService: NbSearchService) { this.searchService.onSearchSubmit()
    .subscribe((data: any) => {
      this.value = data.term;
    });
   }
}
