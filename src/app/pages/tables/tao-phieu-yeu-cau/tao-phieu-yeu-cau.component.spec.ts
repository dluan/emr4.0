import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaoPhieuYeuCauComponent } from './tao-phieu-yeu-cau.component';

describe('TaoPhieuYeuCauComponent', () => {
  let component: TaoPhieuYeuCauComponent;
  let fixture: ComponentFixture<TaoPhieuYeuCauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaoPhieuYeuCauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaoPhieuYeuCauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
