import { icons } from 'eva-icons';
import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-tao-phieu-yeu-cau',
  templateUrl: './tao-phieu-yeu-cau.component.html',
  styleUrls: ['./tao-phieu-yeu-cau.component.scss']
})
export class TaoPhieuYeuCauComponent implements OnInit {
  
  ngOnInit() {
  }
  settings = {
    actions: false,
    columns: {
      id: {
        title: 'STT',
        type: 'number',
      },
      IDPatient: {
        title: 'Mã Số ',
        type: 'number',
      },
      firstName: {
        title: 'Tên Người Bệnh',
        type: 'string',
      },
      treatmenttype: {
        title: 'Loại Điều Trị',
        type: 'string',
      },
      ReportType: {
        title: 'Loại Bệnh Án',
        type: 'string',
      },
      ChuyenKHoa: {
        title: 'Chuyên Khoa',
        type: 'string',
      },
      SickID: {
        title: 'Mã Bệnh',
        type: 'number',
      },
      TreatmentTime: {
             title: 'Thời Gian Điều Trị',
             type: 'Number',
      },
      version: {
        title: 'Phiên bản',
        type: 'string',
      },
      icons: {
        type: 'html',
      },

    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      edit: false,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
  };

  source: LocalDataSource = new LocalDataSource();source1: LocalDataSource = new LocalDataSource();
  value = 'Tìm kiếm........';
  constructor(private service: SmartTableData, private router: Router) {
    const data = this.service.getData();

    this.source.load(data);
    const data1 = this.service.get1Data();
    this.source1.load(data1);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      {
        field: 'stt',
        search: query,
      },
      {
        field: 'ten_truy_cap',
        search: query,
      },
      {
        field: 'ten_nguoi_dung',
        search: query,
      },,
      {
        field:'chuc_danh',
        search: query,
      },
      {
        field: 'email',
        search: query,
      },
      {
        field: 'trang_thai',
        search: query,
      },
      {
        field: 'nhom_nguoi_dung',
        search: query,
      },
    ], false);
    }
    async ChiTiet() {
      this.router.navigateByUrl('/pages/thong-tin-phieu');
  }
}
