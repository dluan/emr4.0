import { NbCardModule } from '@nebular/theme';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LapTomTatMainComponent } from './lap-tom-tat-main.component';

@NgModule({
  imports: [
    CommonModule,
    NbCardModule
  ],
  declarations: [LapTomTatMainComponent],
  exports: [LapTomTatMainComponent],
})
export class LapTomTatMainModule { }
