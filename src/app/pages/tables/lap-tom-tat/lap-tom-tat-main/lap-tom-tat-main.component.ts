import { Component} from '@angular/core';
import { NbSearchService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-lap-tom-tat-main',
  templateUrl: './lap-tom-tat-main.component.html',
  styleUrls: ['./lap-tom-tat-main.component.scss'],
})
export class LapTomTatMainComponent {
  value = 'Tìm kiếm.....';
  constructor(private searchService: NbSearchService, private router: Router) { this.searchService.onSearchSubmit()
    .subscribe((data: any) => {
      this.value = data.term;
    });
   }
   async list() {
     this.router.navigateByUrl('/pages/lap-tom-tat-list');
   }
}
