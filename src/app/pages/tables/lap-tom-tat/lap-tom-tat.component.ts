import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-lap-tom-tat',
  templateUrl: './lap-tom-tat.html',
  styleUrls: ['./lap-tom-tat.scss'],
})
export class LapTomTatComponent implements OnInit{
  public isCard: boolean;
  public isList: boolean;
  ngOnInit() { this.isCard = true; this.isList = false; }
  public Card(): void { this.isCard = true; this.isList = false; }
  public List(): void { this.isList = true; this.isCard = false;  }
}
