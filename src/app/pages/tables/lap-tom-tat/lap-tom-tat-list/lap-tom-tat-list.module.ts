import { NbCheckboxModule } from '@nebular/theme';
import { LapTomTatListComponent } from './lap-tom-tat-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    NbCheckboxModule,
  ],
  declarations: [LapTomTatListComponent],
  exports: [LapTomTatListComponent],
})
export class LapTomTatListModule { }
