import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from './../../../../@core/data/smart-table';
import { Component } from '@angular/core';
import { NbSearchService } from '@nebular/theme';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-lap-tom-tat-list',
  templateUrl: './lap-tom-tat-list.component.html',
  styleUrls: ['./lap-tom-tat-list.component.scss'],
})

export class LapTomTatListComponent {

  settings = {
    actions: false,
    columns: {
      id: {
        title: 'STT',
        type: 'number',
      },
      codeuser: {
        title: 'Mã người bệnh',
        type: 'number',
      },
      medicalcode: {
        title: 'Số bệnh án',
        type: 'number',
      },
      username: {
        title: 'Tên người bệnh',
        type: 'string',
      },
      treatmenttype: {
        title: 'Loại điều trị',
        type: 'string',
      },
      medicaltype: {
        title: 'Loại bệnh án',
        type: 'string',
      },
      department: {
        title: 'Chuyên khoa',
        type: 'string',
      },
      diseasecode: {
        title: 'Mã bệnh',
        type: 'number',
      },
      time: {
        title: 'Thời gian điều trị',
        type: 'string',
      },
      version:{
        title: 'Phiên bản',
        type: 'number',
      },
      status: {
        title: 'Trạng thái',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  value = 'Tìm kiếm.....';
  constructor(private service: SmartTableData, private searchService: NbSearchService) {
    const data = this.service.getData();
    this.source.load(data);
    this.searchService.onSearchSubmit()
// tslint:disable-next-line: no-shadowed-variable
    .subscribe((data: any) => {
      this.value = data.term;
    });
  }

}
