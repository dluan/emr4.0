import { LapTomTatComponent } from './lap-tom-tat.component';
import { LapTomTatMainModule } from './lap-tom-tat-main/lap-tom-tat-main.module';
import { LapTomTatListModule } from './lap-tom-tat-list/lap-tom-tat-list.module';
import { ThemeModule } from './../../../@theme/theme.module';
import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { LapTomTatRoutingModule, routedComponents } from './lap-tom-tat-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { NbCheckboxModule, NbCardModule } from '@nebular/theme';

const routes: Routes = [
  {
    path: '',
    component: LapTomTatComponent,
  },
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes),
    LapTomTatRoutingModule,
    LapTomTatMainModule,
    LapTomTatListModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbCheckboxModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class LapTomTatModule { }
