import { LapTomTatMainModule } from './lap-tom-tat-main/lap-tom-tat-main.module';
import { LapTomTatListModule } from './lap-tom-tat-list/lap-tom-tat-list.module';
import { LapTomTatMainComponent } from './lap-tom-tat-main/lap-tom-tat-main.component';
import { LapTomTatListComponent } from './lap-tom-tat-list/lap-tom-tat-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LapTomTatComponent } from './lap-tom-tat.component';

const routes: Routes = [{
  path: '',
  component: LapTomTatComponent,
  children: [
    {
    path: 'lap-tom-tat-list',
    component: LapTomTatListComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes), LapTomTatListModule],
  exports: [RouterModule],
})
export class LapTomTatRoutingModule { }

export const routedComponents = [
  LapTomTatComponent,
  LapTomTatListComponent,
];
