import { Router } from '@angular/router';
import { Component, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { NbAuthService } from '../services/auth.service';
import { takeWhile } from 'rxjs/operators';


@Component({
  selector: 'nb-auth',
  styleUrls: ['./auth.component.scss'],
  templateUrl: './auth.component.html',
  
})
export class NbAuthComponent implements OnDestroy {
  namecompany="Made by FPT Information System Corp.";
  private alive = true;

  subscription: any;

  authenticated: boolean = false;
  token: string = '';

  // showcase of how to use the onAuthenticationChange method
  constructor(protected auth: NbAuthService, protected location: Location,private router:Router) {

    this.subscription = auth.onAuthenticationChange()
      .pipe(takeWhile(() => this.alive))
      .subscribe((authenticated: boolean) => {
        this.authenticated = authenticated;
      });
  }

  back() {
    this.location.back();
    return false;
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
